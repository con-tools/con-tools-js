/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Users API
 * @param ConTroll api
 */
var ConTrollUsers = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'users'};
	return this;
};

/**
 * Retrieve all user profiles in the system
 * @return a promise that will resolve with the list of user profile objects
 */
ConTrollUsers.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Retrieve a user report for the specified user
 * @param id User ID to retrieve
 * @return a promise that will resolve with the requested user profile object
 */
ConTrollUsers.prototype.get = function(id) {
	return this.api.get(this.collection, id);
};

/**
 * Create a new user profile
 * @param name String full name of the user
 * @param email String email address of the user
 * @return a promise that will resolve with the new user profile object
 */
ConTrollUsers.prototype.create = function(name, email) {
	return this.api.create(this.collection, {
		name: name,
		email: email,
	});
}

module.exports = ConTrollUsers;
