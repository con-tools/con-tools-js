/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Conventions API
 * @param ConTroll api
 */
var ConTrollConventions = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'conventions'};
	return this;
};

/**
 * List all conventions
 * @return a promise that will resolve to the list of all convention objects managed
 *   by the current user, including their keys
 */
ConTrollConventions.prototype.catalog = function() {
	return this.api.get(this.collection, '?keys=1&manager=1');
};

/**
 * Create a new convention
 * @param title Title of the convention
 * @param series Name of a convention series if this instance is a yearly convention
 * @param location Physical location (such as an address) where the convention will be held
 * @param website URL of the convention website
 * @return a promise that will resolve to the newly created convention object
 */
ConTrollConventions.prototype.create = function(title, series, location, website) {
	var data = {
			title: title,
			series: series,
			location: location,
			website: website
	};
	return this.api.create(this.collection, data);
};

/**
 * Retrieve the current convention's details
 * @return a promise that will resolve to the currently selected convnetion object
 */
ConTrollConventions.prototype.getCurrent = function() {
	return this.api.get(this.collection, 'self');
};

/**
 * Update convention settings
 * @param id convention ID to update
 * @param data property list with data to update
 * @return a promise that will resolve to the updated convention object
 */
ConTrollConventions.prototype.update = function(id, data) {
	return this.api.update(this.collection, id, data);
};

module.exports = ConTrollConventions;
