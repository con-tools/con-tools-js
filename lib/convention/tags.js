/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll System Tags API
 * @param ConTroll api
 */
var ConTrollTags = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'tagtypes'};
	return this;
};

/**
 * Retrieve the list of system tags
 * @return a promise that will resolve to the list of system tag objects
 */
ConTrollTags.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Return a specific system tag
 * @param title String title or slug of the system tag to retrieve
 * @return a promise that will resolve to the system tag object
 */
ConTrollTags.prototype.getType = function(title) {
	return this.api.get(this.collection, title);
};

/**
 * Add a system tag to the convention
 * @param title String title of the system tag to add
 * @param requirement String the type of requirement for this tag. One of:
 *  * Optional
 *  * One
 *  * Multiple
 * @param ispublic Boolean whether the new tag is to be exposed to non-managers
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.addType = function(title, requirement, ispublic) {
	return this.api.create(this.collection, {
		title: title,
		requirement: requirement,
		"public": ispublic
	});
};

/**
 * Update an existing system tag
 * @param oldtitle String title or slug of the system tag to update
 * @param newtitle String update title to change to
 * @param requirement String updated requirement to change to (see #addType for details)
 * @param ispublic Boolean new public/private state to set
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.updateType = function(oldtitle, newtitle, requirement, ispublic) {
	return this.api.update(this.collection, oldtitle, {
		title: newtitle,
		requirement: requirement,
		"public": ispublic
	});
};

/**
 * Remove and existing system tag
 * @param title String title or slug of the system tag to remove
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.deleteType = function(title) {
	return this.api.del(this.collection, title);
};

/**
 * Replace an option value in an existing system tag
 * @param title String title or slug of the system tag where a value is to be updated
 * @param oldvalue String old value to replace
 * @param newvalue String new value to use instead
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.replaceValue = function(title, oldvalue, newvalue) {
	var replace_values = {};
	replace_values[oldvalue] = newvalue;
	return this.api.update(this.collection, title, {
		"replace-values": replace_values
	});
};

/**
 * Add a new option value to an existing system tag
 * @param title String title or slug of the system tag where a value is to be updated
 * @param newvalue String new value to add as an option to the system tag
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.addValue = function(title, newvalue) {
	return this.api.update(this.collection, title, {
		values: [ newvalue ]
	});
};

/**
 * Delete an existing option on an existing system tag
 * @param title String title or slug of the system tag where a value is to be updated
 * @param value String the existing option value to remove
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.deleteValue = function(title, value) {
	return this.api.update(this.collection, title, {
		"remove-values": [ value ]
	});
};

module.exports = ConTrollTags;
