/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Coupons API
 * @param ConTroll api
 */
var ConTrollCoupons = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'coupons'};
	return this;
};

/**
 * Retrieve a list of all coupons in the convention
 * @return a promise that will resolve to the list of coupon objects
 */
ConTrollCoupons.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Retrieve a list of all coupons for a specified coupon type
 * @param typeId Number id of the coupon type
 * @return a promise that will resolve to the list of coupon objects for the specifeid
 *   type
 */
ConTrollCoupons.prototype.forType = function(typeId) {
	return this.api.get(this.collection, { type: typeId });
};

/**
 * Add a new coupon for a user
 * @param typeId Number id of the coupon type to add
 * @param user String|Number id or email of the user to add a coupon for
 * @return a promise that will resolve to the new coupon object created
 */
ConTrollCoupons.prototype.add = function(typeId, user) {
	var data = {
			type: typeId,
			user: user
		};
	return this.api.create(this.collection, data);
};

/**
 * Remove an existing coupon from the convention
 * @param id Number id of the coupon to remove
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollCoupons.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
};

module.exports = ConTrollCoupons;
