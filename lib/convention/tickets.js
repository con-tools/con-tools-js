/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Tickets API
 * @param ConTroll api
 */
var ConTrollTickets = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'tickets'};
	return this;
};

/**
 * Retrieve all tickets authorized for the specified event
 * @param eventId Number id of the event to list tickets for
 * @return a promise that will resolve to the list of ticket objects
 */
ConTrollTickets.prototype.forEvent = function(eventId) {
	return this.api.get(this.collection, '?all=1&is_valid=1&by_event=' + eventId);
};

/**
 * Retrieve all tickets authorized for the specified time slot
 * @param timeslotId Number id of the timeslot to list tickets for
 * @return a promise that will resolve to the list of ticket objects
 */
ConTrollTickets.prototype.forTimeslot = function(timeslotId) {
	return this.api.get(this.collection, '?all=1&is_valid=1&by_timeslot=' + timeslotId);
};

/**
 * Retrieve all tickets authorized for the specified user
 * @param userId Number id of the user to list tickets for
 * @return a promise that will resolve to the list of ticket objects
 */
ConTrollTickets.prototype.forUser = function(userId) {
	return this.api.get(this.collection, '?all=1&is_valid=1&by_user=' + userId);
};

/**
 * Retrieve all tickets authorized in the convention
 * @return a promise that will resolve to the list of ticket objects
 */
ConTrollTickets.prototype.catalog = function() {
	return this.api.get(this.collection, '?all=1&is_valid=1');
};

/**
 * Register a ticket for the specified user, on the specified timeslot with 
 * the specified pass.
 * @param user Number|String id or email of the user to register a ticket for
 * @param timeslot Number id of the timeslot to register a ticket for
 * @param passId Number|Array[Number] id of the pass or passes to register tickets for
 * @return a promise that will resolve to the newly created ticket or a list of ticktes
 */
ConTrollTickets.prototype.create = function(user, timeslot, passId) {
	var data = {
			timeslot: timeslot,
			user_passes: passId,
			user: user
	};
	return this.api.create(this.collection, data);
};

/**
 * Remove a ticket from registration
 * @param id Number id of the ticket to cancel
 * @para, refundTypeId Number id of the coupon type to create as a refund, if needed
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollTickets.prototype.remove = function(id, refundTypeId) {
	var query = id;
	if (refundTypeId) 
		query += '?refund-coupon-type='+ refundTypeId;
	return this.api.del(this.collection, query);
};

module.exports = ConTrollTickets;
