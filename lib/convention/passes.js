/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Pass API
 * @param ConTroll api
 */
const ConTrollPasses = function(api) {
	this.api = api;
	this.collection = { convention: true, collection: 'passes' };
	return this;
};

/**
 * Retrieve the list of all available passes (including non-public ones)
 * @return a promise that will resolve to the list of pass objects
 */
ConTrollPasses.prototype.catalog = function() {
	return this.api.get(this.collection, '?all=1');
};

module.exports = ConTrollPasses;
