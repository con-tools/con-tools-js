/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Time Slots API
 * @param ConTroll api
 */
var ConTrollTimeslots = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'timeslots'};
	return this;
};

/**
 * Retrieve all timeslots for the convention
 * @return a promise that will resolve to a list of timeslot objects
 */
ConTrollTimeslots.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Retrieve all timeslots for a specific event
 * @param event_id Number id of the event to retrieve timeslots for
 * @return a promise that will resovle to a list of timeslot objects
 */
ConTrollTimeslots.prototype.forEvent = function(event_id) {
	return this.api.get(this.collection, { event: event_id });
};

/**
 * Create a new timeslot for an event
 * @param event_id Number id of the event to create a timeslot for
 * @return a promise that will resolve to the created timeslot object
 */
ConTrollTimeslots.prototype.create = function(event_id, start_time, duration, locations, hosts) {
	var data = {
			event: event_id,
			start: parseInt(start_time.getTime()/1000),
			duration: duration,
			locations: locations
		};
	if (hosts && hosts.length > 0)
		// submit hosts directly - assumes a host is an object with either email or id fields and optionally a name - like api wants 
		data.hosts = hosts; 
	return this.api.create(this.collection, data);
};

/**
 * Remove an existing timeslot
 * @param id Number id of the timeslot to remove
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTimeslots.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
};

/**
 * Update the timeslot location list
 * @param id Number id of the timeslot to update
 * @param toAdd Array[Object|String] list of location objects or location slugs to add
 *   to the timeslot
 * @param toRemove Array[Object|String] list of location objects or location slugs to
 *   remove from the timeslot
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTimeslots.prototype.updateLocations = function(id, toAdd, toRemove) {
	return this.api.update(this.collection, id, {
		'locations': toAdd.map(function(l){ return l.slug ? l.slug : l; }),
		'remove-locations': toRemove.map(function(l){ return l.slug ? l.slug : l; })
	});
};

/**
 * Update the host list for the timeslot
 * @param id Number id of the timeslot to update
 * @param toAdd Array[Object] list of timeslot host objects to add to the timeslot
 * @param toRemove Arrau[Object] list of timeslot host objects to remove from the
 *   timeslot
 * @return a promise that will resolve to a property list with a boolean status field 
 */
ConTrollTimeslots.prototype.updateHosts = function(id, toAdd, toRemove) {
	return this.api.update(this.collection, id, {
		'hosts': toAdd,
		'remove-hosts': toRemove
	});
};

module.exports = ConTrollTimeslots;
