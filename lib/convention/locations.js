/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Locations API
 * @param ConTroll api
 */
var ConTrollLocations = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'locations'};
	return this;
};

/**
 * Retrieve the list of all locations in the current convention
 * @return a promise that will resolve to the list of location objects
 */
ConTrollLocations.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Add a new location to the convention
 * @param title String title for the new location
 * @param area String area description for the new location
 * @param max_attendees Number maximum number of attendees the new location supports
 * @return a promise that will resolve to the new location object created
 */
ConTrollLocations.prototype.add = function(title, area, max_attendees) {
	return this.api.create(this.collection, {
		title: title,
		area: area,
		"max-attendees": max_attendees
	});
}

/**
 * Update an existing location in the convention
 * @param id Number id of the location to update
 * @pram title String new title to set for this location
 * @param area String new area description to set for this location
 * @param max_attendees Number new maximum number of attendees allowed in this location
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollLocations.prototype.update = function(id, title, area, max_attendees) {
	return this.api.update(this.collection, id, {
		title: title,
		area: area,
		"max-attendees": max_attendees
	});
}

/**
 * Remove an existing location from the convention
 * @param id Number id of the location to remove
 * @return a promise that will resolve to a property list contianing a boolean status field
 */
ConTrollLocations.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
}

module.exports = ConTrollLocations;
