/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Managers API
 * @param ConTroll api
 */
var ConTrollManagers = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'managers'};
	return this;
};

/**
 * Return a list of all convention management users (for all roles)
 * @return a promise that will resolve with the list of all management objects
 */
ConTrollManagers.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Add a new management role to the convention
 * @param userObj Object a management object to create
 * @return a promise that will resolve with the new management object
 */
ConTrollManagers.prototype.add = function(userObj) {
	return this.api.create(this.collection, userObj);
};

/**
 * Remove a management role from the convention
 * @param id Number id of management object to remove
 * @return a promise that will resolve with a property list containing a boolean status field
 */
ConTrollManagers.prototype.remove = function(id) {
	this.api.del(this.collection, id);
};

module.exports = ConTrollManagers;
