'use strict';

const rollbar = require('rollbar-browser');

const rollbarConfig = {
	accessToken: "35db6413e9984266a607b2c31e376db3",
	captureUncaught: true,
	captureUnhandledRejections: false,
	payload: {
		environment: ((window||{}).location||{}).host||'local',
	}
};

const Rollbar = rollbar.init(rollbarConfig);
const ctr = new (require('./base'))(window);
ctr.Rollbar = Rollbar;

module.exports = ctr;
