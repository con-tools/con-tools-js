'use strict';

const rollbar = require("rollbar");
rollbar.init("cc7005ece2b8447bac2a05965ea935ac", {
  environment: "production"
});

const ConTroll = require('./base');

module.exports = function(endpoint) {
	let ctr = ConTroll(null, endpoint);
	ctr.Rollbar = rollbar;
	return ctr;
};
